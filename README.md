# CliMetLab + Animations Tutorial

This repository contains a tutorial.ipynb which is intended to introduce people to CliMetLab and matplotlib animations. The tutorial uses CliMetLab to download a few examples of AOS data and two examples of how to use matplotlib to make animations. From the CliMetLab docs: "CliMetLab is a Python package aiming at simplifying access to climate and meteorological datasets, allowing users to focus on science instead of technical issues such as data access and data formats." Once we have the data we need, it's often useful to make animations to help visualize data over time. Thus, this tutorial also covers how to make basic animations using matplotlib.

